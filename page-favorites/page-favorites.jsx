import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import bem from 'bem';
import * as redux from '../data-storage/redux';
import BookThumb from '../part-book-thumb';

import styles from './page-favorites.scss';

@connect(({ [redux.key]: { favorites } }) => ({ favorites }), redux)
@bem
export default class Favorites extends Component {
  static propTypes = {
    favorites: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      author: PropTypes.string,
      authorId: PropTypes.number
    }))
  };

  render() {
    const { favorites = [] } = this.props;
    return (
      <div data-block="favoritesPage" data-bemstyles={styles}>
        <Helmet>
          <title>Избранное</title>
        </Helmet>
        <div data-elem="favoritesList">
          {!isEmpty(favorites) && favorites.map(({ id, title, author, authorId } = {}) => (
            <BookThumb
              key={id}
              id={id}
              title={title}
              author={author}
              authorId={authorId}
            />
          ))}
          {isEmpty(favorites) && <h1 data-elem="emptyFavorites">В избранном пока ничего нет</h1>}
        </div>
      </div>
    );
  }
}
