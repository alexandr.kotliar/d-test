**Котляра А.К.**
**Магазин книг**

*install*
 1. mkdir kotliar_test
 2. cd kotliar_test
 3. git clone git@gitlab.com:dryymoon/dryymoon-react-boilerplate.git
 4. cd dryymoon-react-boilerplate
 5. npm i
 6. cd  src-sites
 7. git clone git@gitlab.com:akotliar/d-test.git
 8. cd d-test
 9. npm i
 10. cd ../../
 11. npm run dev-cli
 12. Open in browser projects admin page [localhost:3000/devcli](http://localhost:3000/devcli)
 13. Check d-test project to build
 14. Navigate to project link, you can find it on projects admin page


**requirements: npm > 6.4, node 10.15.x**