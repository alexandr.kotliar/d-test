import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link, { linkDecorator } from 'components/link';
import Helmet from 'react-helmet';
import Button from 'components/button';
import preload from 'preload';
import api from 'api';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import bem from 'bem';
import { connect } from 'react-redux';
import * as redux from '../data-storage/redux';
import StarIcon from '../assets/StarIcon.svg';

import styles from './page-book.scss';

@connect(({ [redux.key]: { favorites } }) => ({ favorites }), redux)
@preload(({ pathname }) => ({ $injectRaw: true, pathname }))
@api
@bem
@linkDecorator
export default class Book extends Component {
  static propTypes = {
    pathname: PropTypes.string,
    addToFavorites: PropTypes.func,
    favorites: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      author: PropTypes.string,
      authorId: PropTypes.number
    }))
  };

  constructor(props, ctx) {
    super(props, ctx);
    this.state = {
      book: {},
      hasError: false,
    };
    this.makeDataReq = this.makeDataReq.bind(this);
  }

  componentDidMount() {
    this.makeDataReq();
  }

  componentWillReceiveProps(nextProps) {
    const { pathname: nextPath } = nextProps;
    const { pathname } = this.props;
    if (!isEqual(pathname, nextPath)) {
      this.makeDataReq();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(this.props, nextProps) || !isEqual(this.state, nextState);
  }

  makeDataReq() {
    const { pathname } = this.props;
    this.api.get(`/api${pathname}`).then(({ list: book }) => {
      this.setState({ book, hasError: false });
    }).catch(() => {
      this.setState({ book: {}, hasError: true });
    });
  }

  render() {
    const {
      addToFavorites,
      favorites = [],
    } = this.props;
    const { book, hasError } = this.state;
    const { id, title, descr, genreId, genreName, author, authorId } = book;
    const isFavorited = find(favorites, { id });
    const authorLink = `/author/${authorId}`;
    return (
      <div data-block="bookPage" data-bemstyles={styles}>
        {title && (
          <Helmet>
            <title>{title}</title>
          </Helmet>
        )}
        {hasError && <h1 data-elem="errorText">К сожалению, книга не нашлась</h1>}
        {!isEmpty(book) &&
        <div data-elem="content">
          {title && author && <h1 data-elem="bookTitle">{title}</h1>}
          {authorLink && <Link to={authorLink} data-elem="authorLink" key={`linkToAuthor${id}`}>
            {author}
          </Link>}
          {genreId && <Link to={`/genre/${genreId}`} data-elem="genreLink" key={`linkToGenre${id}`}>
            {genreName}
          </Link>}
          {descr && <div data-elem="descr">{descr}</div>}
          {id && title && author && authorId &&
          <Button
            data-elem="favoriteButton"
            disabled={false}
            onClick={() => addToFavorites({ id, title, author, authorId })}>
            <span>
              <StarIcon data-mods={{ colored: isFavorited }} />
            </span>
          </Button>}
        </div>
        }
      </div>
    );
  }
}
