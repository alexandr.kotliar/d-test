import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import Link from 'components/link';
import Button from 'components/button';
import DropDown from 'components/drop-down';

import styles from './part-author-thumb.scss';

@bem
export default class AuthorThumb extends Component {
  static propTypes = {
    author: PropTypes.string,
    id: PropTypes.number,
    books: PropTypes.arrayOf(PropTypes.shape({
      bookId: PropTypes.number,
      title: PropTypes.string
    }))
  };

  constructor(props) {
    super(props);
    this.renderAuthorsBooks = this.renderAuthorsBooks.bind(this);
  }

  renderAuthorsBooks() {
    const { books = [] } = this.props;
    return (
      <div data-elem="authorBooksLinks">
        {books.map(({ bookId, title } = {}) => {
          const bookLink = `/book/${bookId}`;
          return (
            <div data-elem="authorBookLink" key={`authorBookLinks${bookId}`}>
              <Link to={bookLink}>
                {title}
              </Link>
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    const { author = '', id, books = [] } = this.props;
    const authorLink = `/author/${id}`;
    return (
      <div data-block="authorThumb" data-bemstyles={styles}>
        <DropDown
          data-elem="dropDown"
          onClose={this.onCloseDD}
          ref={ref => this.ddRef = ref}>
          <Button data-elem="button">
            <div data-elem="buttonLabel">
              {author}
            </div>
          </Button>
          <div data-elem="ddContent">
            <div data-elem="authorLink">
              <Link to={authorLink} data-elem="registrationLink" key="linkToRegistration">
                Перейти на страницу автора
              </Link>
            </div>
            {(books.length > 0) &&
            <div data-elem="authorsBooks">Книги автора</div>
            }
            {(books.length > 0) && this.renderAuthorsBooks()}
          </div>
        </DropDown>
      </div>
    );
  }
}
