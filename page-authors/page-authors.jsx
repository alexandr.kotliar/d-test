import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import bem from 'bem';
import preload from 'preload';
import AuthorThumb from './part-author-thumb';
import Paginator, { paginator } from '../part-paginator';

import styles from './page-authors.scss';

@paginator({
  defaultPage: 1,
  defaultLimit: 20,
  availableLimits: [20, 50, 100],
  pageName: 'pageAuthors',
})
@preload(({ props: { pageLimit, pageOffset } }) => ({
  authorsData: `/api/authors?offset=${pageOffset}&limit=${pageLimit}`,
}))
@bem
export default class Authors extends Component {
  static propTypes = {
    authorsData: PropTypes.shape({
      authors: PropTypes.arrayOf(PropTypes.shape({
        author: PropTypes.string,
        id: PropTypes.number,
        books: PropTypes.arrayOf(PropTypes.shape({
          bookId: PropTypes.number,
          title: PropTypes.string
        }))
      })),
      total: PropTypes.number
    })
  };

  render() {
    const { authorsData: { list: authors = [], total = 0 } = {} } = this.props;
    return (
      <div data-block="authorsPage" data-bemstyles={styles}>
        <Helmet>
          <title>Авторы</title>
        </Helmet>
        <div data-elem="authorsList">
          {authors.map(({ author, id, books }) => (
            <AuthorThumb
              key={id}
              author={author}
              id={id}
              books={books}
            />
          ))}
        </div>
        <Paginator {...this.props} countItems={total} />
      </div>
    );
  }
}
