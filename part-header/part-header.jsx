import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import isEmpty from 'lodash/isEmpty';
import Link from 'components/link';
import { connect } from 'react-redux';
import * as redux from '../data-storage/redux';

import styles from './part-header.scss';

@connect(({ [redux.key]: { favorites } }) => ({ favorites }))
@bem
export default class Header extends Component {
  static propTypes = {
    favorites: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      author: PropTypes.string,
      authorId: PropTypes.number
    }))
  };

  render() {
    const { favorites = [] } = this.props;
    return (
      <div data-block="header" data-bemstyles={styles}>
        <div data-elem="menuItem">
          <Link to="/" data-elem="registrationLink" key="linkToRegistration">
            Книги
          </Link>
        </div>
        <div data-elem="menuItem">
          <Link to="/authors" data-elem="registrationLink" key="linkToRegistration">
            Авторы
          </Link>
        </div>
        <div data-elem="menuItem">
          <Link to="/favorites" key="linkToRegistration">
            <span date-elem="favoritesWord">Избранное</span>
            {!isEmpty(favorites) &&
            <span date-elem="favoritesCount"> - {favorites.length}</span>
            }
          </Link>
        </div>
      </div>
    );
  }
}
