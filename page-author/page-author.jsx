import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'components/link';
import preload from 'preload';
import api from 'api';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import { linkDecorator } from 'components/link';
import Helmet from 'react-helmet';
import bem from 'bem';

import styles from './page-author.scss';

@preload(({ pathname }) => ({ $injectRaw: true, pathname }))
@api
@bem
@linkDecorator
export default class PageAuthor extends Component {
  static propTypes = {
    pathname: PropTypes.string,
    authorInfo: PropTypes.shape({
      author: PropTypes.string,
      biography: PropTypes.string,
      books: PropTypes.arrayOf(PropTypes.shape({
        bookId: PropTypes.number,
        title: PropTypes.string
      })),
    }),
  };

  constructor(props, ctx) {
    super(props, ctx);
    this.state = {
      authorData: {},
      hasError: false,
    };
    this.makeDataReq = this.makeDataReq.bind(this);
  }

  componentDidMount() {
    this.makeDataReq();
  }

  componentWillReceiveProps(nextProps) {
    const { pathname: nextPath } = nextProps;
    const { pathname } = this.props;
    if (!isEqual(pathname, nextPath)) {
      this.makeDataReq();
    }
  }

  makeDataReq() {
    const { pathname } = this.props;
    this.api.get(`/api${pathname}`).then(({ list: authorData }) => {
      this.setState({ authorData, hasError: false });
    }).catch(() => {
      this.setState({ authorData: {}, hasError: true });
    });
  }

  render() {
    const { authorData, hasError } = this.state;
    const { author, biography, books = [] } = authorData;
    return (
      <div data-block="pageAuthor" data-bemstyles={styles}>
        <Helmet>
          <title>{author}</title>
        </Helmet>
        {hasError && <h1 data-elem="errorText">К сожалению, автор потерялся:)</h1>}
        {!isEmpty(authorData) &&
        <div data-elem="content">
          {author && <h1>{author}</h1>}
          {biography && <div data-elem="authorBiography">{biography}</div>}
          {!isEmpty(books) &&
          <div data-elem="authorsBooks">
            <div data-elem="authorsBooksHeader">Книги Автора:</div>
            {books.map(({ bookId, title } = {}) => (
              <div data-elem="authorsBookLink" key={`linkToBook${bookId}`}>
                <Link
                  to="$book"
                  params={{ bookId }}
                >{title}
                </Link>
              </div>
            ))
            }
          </div>
          }
        </div>
        }
      </div>
    );
  }
}
