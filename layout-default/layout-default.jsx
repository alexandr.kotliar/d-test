import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import bem from 'bem';
import './layout-default.scss';
import Header from '../part-header';

@bem
export default class Layout extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  render() {
    const { children } = this.props;
    return (
      <div data-block="default">
        <Helmet>
          <html lang="ru" />
          <title>Book store</title>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
          <meta name="format-detection" content="telephone=no" />
        </Helmet>
        <Header />
        <div data-elem="content">{children}</div>
      </div>
    );
  }
}
