/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import Helmet from 'react-helmet';
import bem from 'bem';
import preload from 'preload';
import BookThumb from '../part-book-thumb';
import Paginator, { paginator } from '../part-paginator';

import styles from './page-home.scss';

@paginator({
  defaultPage: 1,
  defaultLimit: 20,
  availableLimits: [20, 50, 100],
  pageName: 'pageBooks',
})
@preload(({ props: { pageLimit, pageOffset } }) => ({
  booksData: `/api/books?offset=${pageOffset}&limit=${pageLimit}`,
}))
@bem
export default class HomePage extends Component {
  render() {
    const { booksData: { list: books = [], total = 0 } = {}, $loading } = this.props;
    return (
      <div data-block="homePage" data-bemstyles={styles}>
        <Helmet>
          <title>Books</title>
        </Helmet>
        <div data-elem="booksList">
          {books.map(({ id, title, author, authorId }) => (
            <BookThumb
              loading={$loading}
              key={id}
              id={id}
              title={title}
              author={author}
              authorId={authorId}
            />
          ))}
          <Paginator {...this.props} countItems={total} />
        </div>
      </div>
    );
  }
}
