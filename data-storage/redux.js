import ReducersRegistry from 'core/helpers/redux/reducers-registry';
import find from 'lodash/find';
import map from 'lodash/map';

export const key = 'favoritesBook';
const fefState = { favorites: [] };
const ADD_TO_FAVORITE = `${key}/ADD_TO_FAVORITE`;

function reducer(state = fefState, action = {}) {
  const { type } = action;

  switch (type) {
    case ADD_TO_FAVORITE: {
      const { favorites } = state;
      if (find(favorites, action.value)) {
        const filteredFavorites = map(favorites, (it) => {
          if (it.id !== action.value.id) return it;
        }).filter(it => it);
        return { ...{}, favorites: filteredFavorites };
      }
      return { ...{}, favorites: [...state.favorites, action.value] };
    }
    default:
      return state;
  }
}

export const addToFavorites = value => ({ type: ADD_TO_FAVORITE, value });

ReducersRegistry.register({ [key]: reducer });

