/* eslint-disable no-unused-vars, arrow-body-style, padded-blocks */
import fakeratorLib from 'fakerator/dist/locales/ru-RU';
import urlLib from 'url';
import sortBy from 'lodash/sortBy';
import find from 'lodash/find';
import sample from 'lodash/sample';
import map from 'lodash/map';
import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import toSafeInteger from 'lodash/toSafeInteger';
import mock from './helpers/mock';

const fakerator = fakeratorLib();

const booksData = [];
const authorsData = [];

let authorBooks = [];
let tempAuthorData = { id: undefined, author: undefined, biography: undefined, books: [] };
const genres = [{ genreId: 1, genreName: 'Детектив' }, { genreId: 2, genreName: 'Фантастика' }];

fakerator.utimes(fakerator.random.number, 10000, 100, 100000).forEach((id) => {
  const { title, content } = fakerator.entity.post();
  const genre = sample(genres);
  const bookAuthor = fakerator.names.name();
  const book = {
    id,
    title,
    descr: content,
    ...genre
  };
  if (authorBooks.length === 0) {
    book.authorId = id;
    book.author = bookAuthor;
    tempAuthorData.id = id;
    tempAuthorData.author = bookAuthor;
    tempAuthorData.biography = content;
    tempAuthorData.authorId = id;
    authorBooks.push({ bookId: id, title });
  } else if (authorBooks.length === 1) {
    const { author: storedAuthor, authorId: storedAuthorId } = tempAuthorData;
    book.id = id;
    book.author = storedAuthor;
    book.authorId = storedAuthorId;
    authorBooks.push({ bookId: id, title });
    tempAuthorData.books = [...authorBooks];
    authorsData.push(tempAuthorData);
    authorBooks = [];
    tempAuthorData = { id: undefined, author: undefined, books: [] };
  }
  booksData.push(book);
});

mock
  .onGet(/\/api\/books/)
  .reply(async (config) => {

    const {
      query: {
        limit = 30,
        offset = 0, 'order[asc]': orderAsc,
        'order[desc]': orderDesc
      }
    } = urlLib.parse(config.url, true);
    let data = cloneDeep(booksData);
    if (orderAsc) data = sortBy(data, [orderAsc]);
    if (orderDesc) data = sortBy(data, [orderDesc]).reverse();

    return [200, {
      result: {
        list: data.slice(toSafeInteger(offset), toSafeInteger(offset) + toSafeInteger(limit)),
        total: data.length
      },
      success: true
    }];
  });

mock
  .onGet(/\/api\/authors/)
  .reply(async (config) => {

    const { query: { limit = 20,
      offset = 0,
      'order[asc]': orderAsc,
      'order[desc]': orderDesc } } = urlLib.parse(config.url, true);
    let data = cloneDeep(authorsData);
    if (orderAsc) data = sortBy(data, [orderAsc]);
    if (orderDesc) data = sortBy(data, [orderDesc]).reverse();
    return [200, {
      result: {
        list: data.slice(toSafeInteger(offset), toSafeInteger(offset) + toSafeInteger(limit)),
        total: data.length
      },
      success: true
    }];
  });

mock
  .onGet(/\/api\/book\/(\d+)/)
  .reply(async (config) => {
    const { pathname } = urlLib.parse(config.url, true);
    const id = toSafeInteger(pathname.replace(/[^0-9]+/, ''));
    const searchedBook = find(booksData, { id });
    if (searchedBook) {
      return [200, {
        result: {
          list: searchedBook,
          total: searchedBook.length
        },
        success: true
      }];
    }
    return [404, {
      result: {
        errors: [{ code: 'cb78fc2d-b83d-4db2-9f09-d9as4838974d', msg: 'Not found' }],
      },
      success: false
    }];
  });

mock
  .onGet(/\/api\/author\/(\d+)/)
  .reply(async (config) => {
    const { pathname, params } = urlLib.parse(config.url, true);
    const id = toSafeInteger(pathname.replace(/[^0-9]+/, ''));
    const searchedAuthor = find(authorsData, { id });
    if (searchedAuthor) {
      return [200, {
        result: {
          list: searchedAuthor,
          total: searchedAuthor.length
        },
        success: true
      }];
    }
    return [404, {
      result: {
        errors: [{ code: 'cb78fc2d-b83d-4db2-9f09-d9as4838974d', msg: 'Not found' }],
      },
      success: false
    }];
  });

mock
  .onGet(/\/api\/genre\/(\d+)/)
  .reply(async (config) => {
    const { pathname,
      query: {
        limit = 20,
        offset = 0,
        'order[asc]': orderAsc,
        'order[desc]': orderDesc
      } } = urlLib.parse(config.url, true);
    const genreId = toSafeInteger(pathname.replace(/[^0-9]+/, ''));
    const data = cloneDeep(booksData);
    let filteredData = map(data, (it) => {
      if (it.genreId === genreId) return it;
    }).filter(it => it);
    if (!isEmpty(filteredData)) {
      if (orderAsc) filteredData = sortBy(filteredData, [orderAsc]);
      if (orderDesc) filteredData = sortBy(filteredData, [orderDesc]).reverse();
      return [200, {
        result: {
          list: filteredData.slice(toSafeInteger(offset), toSafeInteger(offset) + toSafeInteger(limit)),
          total: filteredData.length
        },
        success: true
      }];
    }
    return [404, {
      result: {
        errors: [{ code: 'cb78fc2d-b83d-4db2-9f09-d9as4838974d', msg: 'Not found' }],
      },
      success: false
    }];
  });
