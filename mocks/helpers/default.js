import mock from './mock';

if (__DEV_CLI__) {
  mock.onAny().reply(500, 'mock dont know about this request');
} else {
  mock.onAny().passThrough();
}
