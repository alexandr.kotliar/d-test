import media from '!!sass-vars-to-js-loader!./media.scss'; // eslint-disable-line
import './theme.scss';
import './default.scss';
import './mocks';
import routes from './routes';


export const apiConfig = {
  // url: 'http://5d9cb44266d00400145c9db1.mockapi.io/api/v1',
  url: '/',
  middlewares: [],
};

export { media, routes };
