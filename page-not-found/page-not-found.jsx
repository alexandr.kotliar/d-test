import React, { Component } from 'react';
import Helmet from 'react-helmet';
import bem from 'bem';

import styles from './page-not-found.scss';

@bem
export default class NotFound extends Component {
  render() {
    return (
      <div data-block="pageNotFound" data-bemstyles={styles}>
        <Helmet>
          <title>404</title>
        </Helmet>
        <h1 data-elem="content">404</h1>
      </div>
    );
  }
}
