import LayoutDefault from './layout-default';

const pageHome = () => import(/* webpackChunkName: "page-home" */'./page-home');
const pageBook = () => import(/* webpackChunkName: "page-book" */'./page-book');
const pageAuthors = () => import(/* webpackChunkName: "page-authors" */'./page-authors');
const pageAuthor = () => import(/* webpackChunkName: "page-author" */'./page-author');
const pageGenre = () => import(/* webpackChunkName: "page-genre" */'./page-genre');
const pageFavorites = () => import(/* webpackChunkName: "page-favorites */'./page-favorites');
const pageNotFound = () => import(/* webpackChunkName: "page-404" */'./page-not-found');

const routes = {
  component: LayoutDefault,
  childRoutes: [
    {
      path: '/',
      getComponent: (_, cb) => pageHome().then(module => cb(null, module.default))
    },
    {
      name: 'book',
      path: '/book/:bookId',
      getComponent: (_, cb) => pageBook().then(module => cb(null, module.default))
    },
    {
      path: '/authors',
      getComponent: (_, cb) => pageAuthors().then(module => cb(null, module.default))
    },
    {
      name: 'author',
      path: '/author/:authorId',
      getComponent: (_, cb) => pageAuthor().then(module => cb(null, module.default))
    },
    {
      path: '/favorites',
      getComponent: (_, cb) => pageFavorites().then(module => cb(null, module.default))
    },
    {
      path: '/genre/:id',
      getComponent: (_, cb) => pageGenre().then(module => cb(null, module.default))
    },
    {
      path: '*',
      getComponent: (_, cb) => pageNotFound().then(module => cb(null, module.default))
    },
  ]
};

export default routes;
