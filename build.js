/* eslint-disable no-param-reassign */
const path = require('path');
const proxy = require('http-proxy-middleware');
const fs = require('fs');

const proxyOptions = proxy({

  target: 'http://localhost:3002',
  router: {
    // 'integration.localhost:3000' : 'http://localhost:8001',  // host only
    // 'staging.localhost:3000'     : 'http://localhost:8002',  // host only
    'localhost:3001/spa'         : 'http://localhost:3002',  // host + path
    // '/rest'                      : 'http://localhost:8004'   // path only
  },
  changeOrigin: true, // for vhosted sites, changes host header to match to target's host
  pathRewrite: function (path, req) {
    return path.replace('/spa', '/ru/v5/payment') },
});

module.exports = {};

module.exports.proxyConfig = ((app, filePath) => {
  app.get('/devcli', (req, res) => {
    fs.readFile(filePath, (err, data) => {
      if (err) throw err;
      res.set('content-type', 'text/html');
      res.send(data);
      res.end();
    });
  });

  app.use('/ru/v5/payment', proxyOptions)

});

module.exports.afterClientConfigure = ({ clientConfig, projectPort }) => {
  clientConfig.output.publicPath = `http://localhost:${projectPort}/assets`;
};