import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import bem from 'bem';
import preload from 'preload';
import api from 'api';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
import BookThumb from '../part-book-thumb';
import Paginator, { paginator } from '../part-paginator';

import styles from './page-genre.scss';

@paginator({
  defaultPage: 1,
  defaultLimit: 20,
  availableLimits: [20, 50, 100],
  pageName: 'pageGenre',
})
@preload(({ pathname }) => ({ $injectRaw: true, pathname }))
@api
@bem
export default class Genre extends Component {
  static propTypes = {
    pathname: PropTypes.string,
    pageLimit: PropTypes.number,
    pageOffset: PropTypes.number,
  };

  constructor(props, ctx) {
    super(props, ctx);
    this.state = {
      booksByGenre: [],
      hasError: false,
      total: 0,
    };
    this.makeDataReq = this.makeDataReq.bind(this);
  }

  componentDidMount() {
    this.makeDataReq();
  }

  componentWillReceiveProps(nextProps) {
    const { pathname: nextPath } = nextProps;
    const { pathname } = this.props;
    if (!isEqual(pathname, nextPath)) {
      this.makeDataReq();
    }
  }

  makeDataReq() {
    const { pathname, pageLimit, pageOffset } = this.props;
    this.api.get(`/api${pathname}?offset=${pageOffset}&limit=${pageLimit}`)
      .then(({ list: booksByGenre, total = 0 }) => {
        this.setState({ booksByGenre, hasError: false, total });
      }).catch(() => {
        this.setState({ booksByGenre: [], hasError: true, total: 0 });
      });
  }

  render() {
    const { total, booksByGenre, hasError } = this.state;
    return (
      <div data-block="pageGenre" data-bemstyles={styles}>
        <Helmet>
          <title>Книги по жанру</title>
        </Helmet>
        {hasError && <h1 data-elem="errorText">К сожалению, такого жанра пока не придумали:)</h1>}
        {!isEmpty(booksByGenre) &&
        <div data-elem="genreBooksList">
          {booksByGenre.map(({ id, title, author, authorId }) => (
            <BookThumb
              id={id}
              key={id}
              title={title}
              author={author}
              authorId={authorId}
            />
          ))}
        </div>
        }
        {total > 0 && <Paginator {...this.props} countItems={total} />}
      </div>
    );
  }
}
