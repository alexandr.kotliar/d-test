import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import find from 'lodash/find';
import Link from 'components/link';
import Button from 'components/button';
import Skeleton from 'react-loading-skeleton';
import { connect } from 'react-redux';
import * as redux from '../data-storage/redux';

import styles from './part-book-thumb.scss';
import StarIcon from '../assets/StarIcon.svg';

@connect(({ [redux.key]: { favorites } }) => ({ favorites }), redux)
@bem
export default class BookThumb extends PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    id: PropTypes.number,
    title: PropTypes.string,
    author: PropTypes.string,
    authorId: PropTypes.number,
    favorites: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      author: PropTypes.string,
      authorId: PropTypes.number
    })),
    addToFavorites: PropTypes.func,
  };

  render() {
    const { id, title, author, authorId, loading, addToFavorites, favorites } = this.props;
    const isFavorited = find(favorites, { id });
    return (
      <div data-block="bookThumb" data-bemstyles={styles}>
        {loading && <Skeleton count={5} />}
        <div data-elem="bookHeader">
          <Button
            data-elem="favoriteButton"
            disabled={false}
            onClick={() => addToFavorites({ id, title, author, authorId })}>
            <span>
              <StarIcon data-mods={{ colored: isFavorited }} />
            </span>
          </Button>
          {id &&
          <div data-elem="bookLink">
            <Link to="$book" params={{ bookId: id }} key={`linkToBook${id}`}>{title}</Link>
          </div>
          }
        </div>
        {authorId &&
        <div data-elem="authorLink">
          <Link to="$author" params={{ authorId }}>{author}</Link>
        </div>
        }
      </div>
    );
  }
}
