/* eslint-disable react/no-multi-comp, jsx-a11y/anchor-has-content */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import { t } from 'c-3po';
import isString from 'lodash/isString';
import isEqual from 'lodash/isEqual';
import Dropdown, { close } from 'components/drop-down';
import { withRouter } from 'react-router';
import urlLib from 'url';
import Link from 'components/link';
import FlexBox from 'components/flexbox';
// import preload from 'preload';
import isBrowser from 'core/helpers/isBrowser';
import styles from './part-paginator.scss';
import ArrowIcon from './assets/arrow-drop.svg';
import ArrowLeftIcon from './assets/arrow-left.svg';
import ArrowRightIcon from './assets/arrow-right.svg';

@withRouter
@ccn
@bem
export default class Paginator extends Component {
  static propTypes = {
    ...shape,
    location: PropTypes.shape({
      pathname: PropTypes.string,
      hash: PropTypes.string,
      query: PropTypes.object // eslint-disable-line
    }).isRequired,
    countItems: PropTypes.number.isRequired,
  };

  static defaultProps = {
    _pageAvailableLimits: [10, 20, 50, 100] // eslint-disable-line
  };

  constructor() {
    super();
    this.handleLinkClick = this.handleLinkClick.bind(this);
    this.savePageLimitToStorage = this.savePageLimitToStorage.bind(this);
  }

  buildPageLinkParams(pageNumber, pageLimit) {
    const { _linkBuilder, location: { pathname, hash, query = {} } } = this.props;
    let linkExtra = urlLib.format({ pathname, hash, query: { ...query, pageNumber, pageLimit } });
    if (_linkBuilder) linkExtra = _linkBuilder(this.props);
    if (isString(linkExtra)) linkExtra = { to: linkExtra };
    return linkExtra;
  }

  calcVisibleNumsOnPage(pageNumber) {
    const { countItems, pageLimit } = this.props;
    let firstVisibleItemNum = ((pageNumber - 1) * pageLimit) + 1;
    if (countItems === 0) firstVisibleItemNum = 0;
    let lastVisibleItemNum = pageNumber * pageLimit;
    if (lastVisibleItemNum > countItems) lastVisibleItemNum = countItems;

    return [firstVisibleItemNum, lastVisibleItemNum];
  }

  savePageLimitToStorage(limit) {
    const { _pageStorageKey } = this.props;
    if (isBrowser && _pageStorageKey) {
      try {
        sessionStorage.setItem(_pageStorageKey, limit);
        localStorage.setItem(_pageStorageKey, limit);
      } catch ({ name, message, stack }) {
        console.error(`${name}:${message}\n${stack}`); // eslint-disable-line no-console
      }
    }
  }

  handleLinkClick() {
    if (this.ddRef) close(this.ddRef);
  }

  renderAvailableSteps() {
    const { _pageAvailableLimits, pageLimit } = this.props;
    if (!_pageAvailableLimits || _pageAvailableLimits.length === 0) return null;
    return _pageAvailableLimits.map(it =>
      (<Link
        data-elem="rangeDropdownItemInline"
        data-mods={{ current: it === pageLimit, notCurrent: it !== pageLimit }}
        disabled={it === pageLimit}
        key={it}
        {...this.buildPageLinkParams(1, it)}
        onClick={() => {
          this.savePageLimitToStorage(it);
          this.handleLinkClick();
        }}>
        {it}
      </Link>));
  }

  renderRangeItemsArr() {
    const { countItems, pageNumber, pageLimit } = this.props;
    const pagesCount = Math.ceil(countItems / pageLimit);
    const arr = [];
    for (let i = 1; i <= pagesCount; i += 1) {
      const [first, last] = this.calcVisibleNumsOnPage(i);
      arr.push(<Link
        data-elem="rangeDropdownItem"
        data-mods={{ current: i === pageNumber, notCurrent: i !== pageNumber }}
        key={i}
        disabled={i === pageNumber}
        {...this.buildPageLinkParams(i, pageLimit)}
        onClick={this.handleLinkClick}>
        {first}-{last}
      </Link>);
    }
    arr.push(<div data-elem="rangeDropdownSeparator" key="separator"/>);
    return arr;
  }

  render() {
    const { countItems, pageNumber = 1, pageLimit } = this.props;
    const [currFirst, currLast] = this.calcVisibleNumsOnPage(pageNumber);
    const isFirstPage = pageNumber === 1;
    let pagesCount = Math.ceil(countItems / pageLimit);
    if (pagesCount < 1) pagesCount = 1;
    if (pageNumber > pagesCount) return null;
    // if (pagesCount < 2) return null; // TODO add Option
    const isLastPage = pageNumber === pagesCount;

    const prevPage = pageNumber > 1 ? pageNumber - 1 : false;
    const nextPage = pageNumber < pagesCount ? pageNumber + 1 : false;

    const renderedRangeItemsArr = this.renderRangeItemsArr();
    const renderedAvailableSteps = this.renderAvailableSteps();

    return (
      <FlexBox row data-block="paginator" data-bemstyles={styles}>
        <Link
          {...this.buildPageLinkParams(prevPage, pageLimit)}
          data-elem="control"
          data-mods={['previous', { mute: isFirstPage }]}
          disabled={isFirstPage}
          onClick={this.handleLinkClick}
          data-test="link__gotoPrevPage">
          <div data-elem="controlIcon">
            <ArrowLeftIcon/>
          </div>
        </Link>
        <Link
          {...this.buildPageLinkParams(nextPage, pageLimit)}
          data-elem="control"
          data-mods={['forward', { mute: isLastPage }]}
          disabled={isLastPage}
          onClick={this.handleLinkClick}
          data-test="link__gotoNextPage">
          <div data-elem="controlIcon">
            <ArrowRightIcon/>
          </div>
        </Link>

        <Dropdown data-elem="rangeDropdown" position="up" ref={node => this.ddRef = node}>
          <button data-elem="rangeDropdownButton">
            <span data-elem="rangeDropdownLabel">
              {currFirst} - {currLast}
            </span>
            <ArrowIcon data-elem="rangeDropdownArrow"/>
          </button>
          <div data-elem="rangeDropdownContent">
            {pagesCount > 1 && renderedRangeItemsArr}
            <div data-elem="rangeDropdownGroupTitle">
              {t`Выводить на странице`}
            </div>
            {renderedAvailableSteps}
          </div>
        </Dropdown>
        {countItems > 0 && (
          <div data-elem="total">
            {t`из`} {countItems}
          </div>
        )}
      </FlexBox>
    );
  }
}

export function paginator({
                            defaultPage,
                            defaultLimit,
                            availableLimits = [],
                            pageName,
                            linkBuilder,
                            linkParser
                          } = {}) {
  if (!defaultPage || !defaultLimit) {
    throw new Error('defaultPage and defaultLimit must be defined for paginator decorator');
  }
  /* if ((!linkBuilder && linkParser) || (linkBuilder && !linkParser)) {
   throw new Error('linkBuilder require linkParser present or vice versa');
   } */

  return function decorator(DecoratedComponent) {
    /* @preload((params) => {
     console.log('Pagintor params', params);
     const storageKey = pageName ? `__paginatorPageLimit_${pageName}` : false;
     let storedPageLimit;
     if (isBrowser && storageKey) {
     try {
     const sessionStoragePageLimit = parseInt(sessionStorage.getItem(storageKey), 10);
     const localStoragePageLimit = parseInt(localStorage.getItem(storageKey), 10);
     storedPageLimit = sessionStoragePageLimit || localStoragePageLimit;
     } catch ({ name, message, stack }) {
     console.error(`${name}:${message}\n${stack}`); // eslint-disable-line no-console
     }
     }

     let { query: { pageNumber, pageLimit } = {} } = params;

     if (linkParser) {
     const parsedLink = linkParser(params);
     pageNumber = parsedLink.pageNumber; // eslint-disable-line
     pageLimit = parsedLink.pageLimit; // eslint-disable-line
     }

     pageNumber = parseInt(pageNumber, 10);
     if (!pageNumber || pageNumber < 1) pageNumber = defaultPage;

     pageLimit = parseInt(pageLimit, 10);
     if (!pageLimit || availableLimits.indexOf(pageLimit) === -1) {
     pageLimit = storedPageLimit || defaultLimit;
     }

     if (pageLimit !== storedPageLimit && isBrowser && storageKey) {
     try {
     sessionStorage.setItem(storageKey, pageLimit);
     } catch ({ name, message, stack }) {
     console.error(`${name}:${message}\n${stack}`); // eslint-disable-line no-console
     }
     }

     const pageOffset = pageLimit * (pageNumber - 1);

     return {
     $injectRaw: true,
     $injectRawSync: true,
     pageNumber,
     pageLimit,
     pageOffset,
     _pageAvailableLimits: availableLimits,
     _pageStorageKey: storageKey,
     _linkBuilder: linkBuilder,
     };
     }) */

    const storageKey = pageName ? `__paginatorPageLimit_${pageName}` : false;

    @withRouter
    class PaginatedComponent extends Component {
      static propTypes = {
        location: PropTypes.shape({
          pathname: PropTypes.string,
          hash: PropTypes.string,
          query: PropTypes.object // eslint-disable-line
        }).isRequired,
      };

      constructor(props) {
        super(props);

        if (isBrowser && storageKey) {
          try {
            const sessionStoragePageLimit = parseInt(sessionStorage.getItem(storageKey), 10);
            const localStoragePageLimit = parseInt(localStorage.getItem(storageKey), 10);
            this.storedPageLimit = sessionStoragePageLimit || localStoragePageLimit;
          } catch ({ name, message, stack }) {
            console.error(`${name}:${message}\n${stack}`); // eslint-disable-line no-console
          }
        }

        this.state = this.calcPageParams(props);
      }

      componentWillReceiveProps(nextProps) {
        const newState = this.calcPageParams(nextProps);
        if (!isEqual(this.state, newState)) this.setState(newState);
      }

      calcPageParams({ props = this.props }) {
        const { location } = props;

        let { query: { pageNumber, pageLimit } = {} } = location;

        if (linkParser) {
          const parsedLink = linkParser(location);
          pageNumber = parsedLink.pageNumber; // eslint-disable-line
          pageLimit = parsedLink.pageLimit; // eslint-disable-line
        }

        pageNumber = parseInt(pageNumber, 10);
        if (!pageNumber || pageNumber < 1) pageNumber = defaultPage;

        pageLimit = parseInt(pageLimit, 10);
        if (!pageLimit || availableLimits.indexOf(pageLimit) === -1) {
          pageLimit = this.storedPageLimit || defaultLimit;
        }

        if (pageLimit !== this.storedPageLimit && isBrowser && storageKey) {
          try {
            sessionStorage.setItem(storageKey, pageLimit);
          } catch ({ name, message, stack }) {
            console.error(`${name}:${message}\n${stack}`); // eslint-disable-line no-console
          }
        }

        const pageOffset = pageLimit * (pageNumber - 1);

        return {
          pageNumber,
          pageLimit,
          pageOffset,
        };
      }

      render() {
        // const { pageNumber, pageLimit, pageOffset } = this.props; // eslint-disable-line
        return (
          <DecoratedComponent
            {...this.props}
            _pageAvailableLimits={availableLimits}
            _pageStorageKey={storageKey}
            _linkBuilder={linkBuilder}
            {...this.state} />
        );
      }
    }

    return PaginatedComponent;
  };
}

export const shape = {
  pageNumber: PropTypes.number.isRequired,
  pageLimit: PropTypes.number.isRequired,
  pageOffset: PropTypes.number.isRequired,
  _pageAvailableLimits: PropTypes.arrayOf(PropTypes.number),
  _pageStorageKey: PropTypes.string,
  _linkBuilder: PropTypes.func,
};
